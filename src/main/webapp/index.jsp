<%-- 
    Document   : index
    Created on : 06-05-2020, 10:21:03
    Author     : kevin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>EMPLEADOS REST</title>
        
    </head>
    <body style="margin: 60px">
        <h1 class="display-4">API-REST EV3: KEVIN NAVA</h1>
        <table class="table table-dark">
                          <thead>
                            <tr>
                              <th scope="col">MÉTODO HTTP</th>  
                              <th scope="col">URL</th>
                              <th scope="col">ACCIÓN</th>
                            </tr>
                          </thead>
                          
                          <tbody>
                            <tr class="shadow">
                                <td>Listar Empleados (GET)</td>
                                <td>/api/personal</td>
                                <td>Colocar sólo URL</td>
                            </tr>
                            
                            <tr class="shadow">
                                <td>Buscar Empleado por Rut (GET)</td>
                                <td>/api/personal/{RUT}</td>
                                <td>Reemplazar {RUT} por el que desea buscar,
                                ejemplo: /api/personal/22.543.656-3</td>
                            </tr>
                            
                            <tr class="shadow">
                                <td>Agregar Empleado (POST)</td>
                                <td>/api/personal</td>
                                <td>Enviar archivo JSON en este formato
                                    (complear los campos ""):
                                {
                                    "apellido": "",
                                    "cargo": "",
                                    "nombre": "",
                                    "rut": ""
                                }</td>
                            </tr>
                            
                            <tr class="shadow">
                                <td>Actualizar Registro (PUT)</td>
                                 <td>/api/personal</td>
                                 <td>Enviar archivo JSON en este formato
                                    (actualizar los campos "" que desee):
                                {
                                    "apellido": "",
                                    "cargo": "",
                                    "nombre": "",
                                    "rut": ""
                                }</td>
                            </tr>
                            
                            <tr class="shadow">
                                <td>Eliminar Registro (DELETE)</td>
                                <td>/api/personal/{RUT}</td>
                                <td>Reemplazar {RUT} por el que desea eliminar,
                                ejemplo: /api/personal/22.543.656-3</td>
                            </tr>  
                          </tbody>
        </table>
    </body>
</html>
