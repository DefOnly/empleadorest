/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.persistence.dao.EmpleadoDAO;
import root.persistence.dao.exceptions.NonexistentEntityException;
import root.persistence.entities.Empleados;

@Path("personal")

public class EmpleadosRest {
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
    EntityManager em;
    EmpleadoDAO dao = new EmpleadoDAO();
    
   @GET
   @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo(){
        List<Empleados> lista = dao.findEmpleadoEntities();
        return Response.ok(200).entity(lista).build();
    }
    
   @GET
   @Path("/{idbuscar}")
   @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") String idbuscar) throws Exception{
        Empleados empleado = dao.findEmpleado(idbuscar);
        return Response.ok(200).entity(empleado).build();
    }
    
   @POST
   @Produces(MediaType.APPLICATION_JSON)
    public String nuevo(Empleados empleado) throws Exception{
        dao.create(empleado);
        
    return "Empleado Guardado";
     
    }
    
    @PUT
    public Response actualizar(Empleados empleado) throws Exception{
        dao.edit(empleado);
       return Response.ok(200).entity(empleado).build();
    }
    
   @DELETE
   @Path("/{iddelete}")
   @Produces(MediaType.APPLICATION_JSON)
      public Response eliminaId(@PathParam("iddelete") String iddelete) throws NonexistentEntityException{
       dao.destroy(iddelete);
       return Response.ok("Empleado Eliminado").build();
    }
}
